import pandas as pd

def clean_descriptions(descriptions):
    '''
    Performs clean up in descriptions.
    '''
    if not isinstance(descriptions, pd.Series):
        descriptions = pd.Series(descriptions)

    # Replaces %20 by space.
    descriptions = descriptions.map(lambda d: d.replace('%20', ' '))
    return descriptions
