'''
Trains a Naive Bayes classifier to classify products from their descriptions.

Usage:
    python3 trainer.py CSV_FILE [OUTPUT_FILE]

    CSV_FILE
        A CSV file with the dataset for training.

    OUTPUT_FILE
        The persisted classifier file name (defaults to 'classifier.pkl').
'''

import sys
import pickle
import pandas as pd
from sklearn.preprocessing import FunctionTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from v360_ml.preprocessing import clean_descriptions

# Reads the CSV file with products data.
data = pd.read_csv(sys.argv[1])

# Creates a transformer to clean up descriptions.
cleanup = FunctionTransformer(clean_descriptions, validate=False)

# Creates a pipeline that creates a bag of words for product descriptions
# and trains a Naive Bayes classifier.
pipeline = Pipeline([
    ('clean', cleanup),
    ('bow', CountVectorizer(strip_accents='ascii', analyzer='word')),
    ('nb', MultinomialNB())
])
classifier = pipeline.fit(data.descricao, data.codigo_barras)

# Persists the classifier to a file.
filename = sys.argv[2] if len(sys.argv) > 2 else 'classifier.pkl'
with open(filename, 'wb') as f:
    pickle.dump(classifier, f)
